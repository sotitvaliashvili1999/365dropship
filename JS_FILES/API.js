import {SERVER_URL} from "./Config.js";

const fetching = async (endPoint) => {
    const response = await fetch(SERVER_URL + endPoint);
    return await response.json();
}
export const getAllProducts = async (sortType) => await fetching(`/products${sortType ? `?sort=${sortType}` : ""}`);
   
export const getCategories = async () => await fetching('products/categories');