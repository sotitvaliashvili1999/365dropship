import { productAmount } from "./Main_Catalog.js";
export const makeProductHTML = item =>
  `           <div class="catalog__product">
                <div class="product__radio">
                    <input type="checkbox" class="product__checkbox--button" name="product__checkbox">
                </div>
                <div class="product__image">
                    <img src="${item.image}" " class="product__image--src">
                </div>
                <div class="product__title">
                    <span class="product__title--name">${item.title}</span> 
                </div>
                <div class="product__price">
                    <span class="product__prices--rrp">RRP: 34$ </span>
                    <span class="product__prices--Profit">Profit: 25% </span>
                    <span class="product__prices--cost">Cost: ${item.price} $</span>
                </div>
            </div>
`;
export const countProductAmount = amount =>
                `<span class="selected-items__amount selected-items__before " 
                  >selected 0 out of ${amount} products</span
                >
                <span class="selected-items__amount selected-items__after "
                  >${amount} products</span
                >`;

export const makeCatalogHtml = list => {
  let Html = "";
  for (const item of list) Html += makeProductHTML(item);
  productAmount.innerHTML = countProductAmount(list.length);
  return Html;
};
