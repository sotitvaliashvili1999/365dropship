import { getAllProducts } from "./API.js";
import { makeCatalogHtml } from "./Generate_Product_HTML.js";

const productCatalog = document.getElementById("productContainer");
export const productAmount = document.getElementById("selected-items__number");
const sortType = document.getElementById("sorting");
const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");

const checkSortType = async sortType => {
  if (sortType === "ascByPrice")
    return getAllProducts().then(list =>
      list.sort((a, b) => a.price - b.price)
    );
  else if (sortType === "descByPrice")
    return getAllProducts().then(list =>
      list.sort((a, b) => b.price - a.price)
    );
  else return getAllProducts(sortType);
};

// fill up catalog;
const fillCatalog = async sortType => {
  productCatalog.innerHTML = makeCatalogHtml(await checkSortType(sortType));
};

fillCatalog();

// sorting by ID;
sortType.addEventListener("change", () => fillCatalog(sortType.value));

// search with sort
searchButton.addEventListener("click", () => {
  checkSortType(sortType.value).then(list => {
    let filteredList = list.filter(item => item.title.toLowerCase().includes(searchQuery.value.toLowerCase()));
    productCatalog.innerHTML = makeCatalogHtml(filteredList);
  });
});
